import Vue from 'vue'
import Router from 'vue-router'
const UserView = () => import('@/views/UserView')

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: 'tdd-app',
  routes: [
    {
      path: '/',
      component: UserView
    }
  ]
})