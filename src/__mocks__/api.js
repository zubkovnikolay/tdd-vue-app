import userFixture from '../../tests/unit/fixtures/userFixture'

export default {
  searchUser: jest.fn().mockResolvedValue(userFixture)
}