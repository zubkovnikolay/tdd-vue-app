import userFixture from '../../../tests/unit/fixtures/userFixture'

export default {
  SEARCH_USER: jest.fn().mockResolvedValue(userFixture)
}